package com.slantedlines.android.horoscopeapp.ui.horoscope;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.slantedlines.android.horoscopeapp.R;
import com.slantedlines.android.horoscopeapp.helper.Horoscope;

import java.util.List;

public class HoroscopeRecyclerAdapter extends RecyclerView.Adapter<HoroscopeRecyclerAdapter.ViewHolder> {
    Context context;
    List<Horoscope> horoscopeList;
    OnHoroscopeSelectListener listener;

    public HoroscopeRecyclerAdapter(Context context, List<Horoscope> horoscopeList, OnHoroscopeSelectListener horoscopeSelectListener) {
        this.context = context;
        this.horoscopeList = horoscopeList;
        this.listener = horoscopeSelectListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.select_horoscope_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        int resID = context.getResources().getIdentifier(horoscopeList.get(position).getmHoroscopeName().toLowerCase(), "drawable", context.getPackageName());
        holder.imageView.setImageResource(resID);
        holder.textViewName.setText(horoscopeList.get(position).getmHoroscopeName());
        holder.textViewRange.setText(horoscopeList.get(position).getmHoroscopeDateRange());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onHoroscopeSelected(horoscopeList.get(position).getmHoroscopeName());
            }
        });
    }

    @Override
    public int getItemCount() {
        return horoscopeList.size();
    }

    public interface OnHoroscopeSelectListener {
        void onHoroscopeSelected(String horoscopeName);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textViewName, textViewRange;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.img_list_item);
            textViewName = itemView.findViewById(R.id.dialog_horos_name);
            textViewRange = itemView.findViewById(R.id.dialog_horos_range);
        }
    }
}
