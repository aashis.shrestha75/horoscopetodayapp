package com.slantedlines.android.horoscopeapp.ui.horoscope;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;


public class ChangeViewModel extends ViewModel {

    private MutableLiveData<String> mutableLiveData = new MutableLiveData<>();

    public LiveData<String> getChangedHoroscope() {
        return mutableLiveData;
    }

    public void setChangedHoroscope(String tempHoroscope) {
        mutableLiveData.setValue(tempHoroscope);
    }
}
