package com.slantedlines.android.horoscopeapp.helper;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.slantedlines.android.horoscopeapp.model_user.LoginResponse;
import com.slantedlines.android.horoscopeapp.model_user.User;
import com.slantedlines.android.horoscopeapp.ui.login.DobActivity;

import java.util.HashMap;

public class MySessionManager {
    // Sharedpref file name
    private static final String PREF_NAME = "HoroscopeTodayPref";

    public static final String KEY_HOROSCOPE = "horoscope";
    private static final String IS_LOGIN = "IsLoggedIn";
    private static final String IS_REGISTERED = "is_registered";
    private static final String KEY_DOB = "dob";

    private static final String KEY_TEMPORARY_FETCH = "temporary_fetch";
    private static final String KEY_TEMPORARY_HOROS = "temporary_horos";
    private static final String IS_STORAGE_LOCATED = "storage_located";


    private static final String KEY_EMAIL = "email";
    private static final String KEY_NAME = "name";
    private static final String KEY_PASS = "password";
    private static final String KEY_PASS_CONFIRM = "password_confirm";
    private static final String KEY_LOGIN_DETAILS = "login_details";
    private static final String KEY_API_TOKEN = "api_token";

    private final String VALUE_EMAIL = "developer@gmail.com";
    private final String VALUE_NAME = "developer";
    private final String VALUE_PASS = "123456";
    private final String VALUE_PASS_CONFIRM = "123456";

    // Shared Preferences
    SharedPreferences pref;
    // Editor for Shared preferences
    SharedPreferences.Editor editor;
    Gson gson;
    // Context
    Context myContext;
    // Shared pref mode
    private int PRIVATE_MODE = 0;

    // Constructor
    public MySessionManager(Context context) {
        this.myContext = context.getApplicationContext();
        pref = myContext.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();

        gson = new Gson();
    }

    /**
     * Create login session
     */
    public void createLoginSession(LoginResponse loginResponse) {
        editor.putString(KEY_API_TOKEN, loginResponse.getAccessToken());

        String jsonUser = gson.toJson(loginResponse);
        editor.putString(KEY_LOGIN_DETAILS, jsonUser);
        // commit changes
        editor.commit();
    }

    public String getHoroscope() {
        return pref.getString(KEY_HOROSCOPE, null);
    }

    public void setHoroscope(String horoscope) {
        editor.putString(KEY_HOROSCOPE, horoscope);
        editor.commit();
    }

    public String getDob() {
        return pref.getString(KEY_DOB, null);
    }

    public void setDob(String dateOfBirth) {
        editor.putString(KEY_DOB, dateOfBirth);
        editor.commit();
    }

    public Boolean getTemporaryFetch() {
        return pref.getBoolean(KEY_TEMPORARY_FETCH, false);
    }

    public void setTemporaryFetch(Boolean isTemporaryHoroscope) {
        editor.putBoolean(KEY_TEMPORARY_FETCH, isTemporaryHoroscope);
        editor.commit();
    }

    public Boolean getStorageLocated() {
        return pref.getBoolean(IS_STORAGE_LOCATED, false);
    }

    public void setStorageLocated(Boolean isLocated) {
        editor.putBoolean(IS_STORAGE_LOCATED, isLocated);
        editor.commit();
    }

    public String getTemporaryHoros() {
        return pref.getString(KEY_TEMPORARY_HOROS, null);
    }

    public void setTemporaryHoros(String temporaryHoroscope) {
        setTemporaryFetch(true);
        editor.putString(KEY_TEMPORARY_HOROS, temporaryHoroscope);
        editor.commit();
    }
    /**
     * Get stored session data
     */
    public LoginResponse getUserDetails() {
        return gson.fromJson(pref.getString(KEY_LOGIN_DETAILS, null), LoginResponse.class);
    }
//    public HashMap<String, User> getUserDetails() {
//        HashMap<String, User> user = new HashMap<>();
//        // user dob
//        user.put(KEY_LOGIN_DETAILS, gson.fromJson(pref.getString(KEY_LOGIN_DETAILS, null), User.class));
//        // return user
//        return user;
//    }

    public void editToday(String newValue) {
        editor.putString(KEY_DOB, newValue);
        editor.commit();
    }

    /**
     * Check login method wil check user login status
     * If false it will redirect user to login page
     * Else won't do anything
     */
    public void checkLogin() {
        // Check login status
        if (!this.isLoggedIn()) {
            // user is not logged in redirect him to Login Activity
            Intent i = new Intent(myContext, DobActivity.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);


            // Staring Login Activity
            myContext.startActivity(i);
        }

    }


    public boolean isLoggedIn() {
        return pref.getBoolean(IS_LOGIN, false);
    }


    public String getApiToken() {
        return pref.getString(KEY_API_TOKEN, "no token");
    }


    public boolean isAppRegistered() {
        return pref.getBoolean(IS_REGISTERED, false);
    }

    public void setAppRegistered() {
        editor.putBoolean(IS_REGISTERED, true);
        editor.commit();
    }

    public String getValueEmail() {
        return VALUE_EMAIL;
    }

    public String getValuePass() {
        return VALUE_PASS;
    }

    public void setLogin(Boolean isLoggedIn) {
        editor.putBoolean(IS_LOGIN, isLoggedIn);
        editor.commit();
    }
}
