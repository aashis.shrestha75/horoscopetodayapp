package com.slantedlines.android.horoscopeapp.ui.login;

import android.app.Application;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.slantedlines.android.horoscopeapp.api.ApiClient;
import com.slantedlines.android.horoscopeapp.api.ApiService;
import com.slantedlines.android.horoscopeapp.helper.InternetDetector;
import com.slantedlines.android.horoscopeapp.helper.MySessionManager;
import com.slantedlines.android.horoscopeapp.model_user.LoginResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginViewModel extends AndroidViewModel {
    private static final String TAG = "LoginViewModel";
    private MutableLiveData<LoginResponse> data;
    private Context context;
    private MySessionManager mySessionManager;

    public LoginViewModel(@NonNull Application application) {
        super(application);
        this.data = new MutableLiveData<>();
        this.context = application;
        this.mySessionManager = new MySessionManager(context);
    }

    public LiveData<LoginResponse> registerApp() {
        if (new InternetDetector(context).hasNetworkConnection()) {
            Call<LoginResponse> callLoginResponse = ApiClient.createService(ApiService.class).login(mySessionManager.getValueEmail(),
                    mySessionManager.getValuePass());
            callLoginResponse.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    if (response.isSuccessful()) {
                        Log.i(TAG, response.body().getAccessToken());
                        data.setValue(response.body());

                    } else {
                        Toast.makeText(context, response.code() + "", Toast.LENGTH_SHORT).show();
                        Log.i(TAG, "ResponseCode=" + response.code());
                        data.setValue(null);
                    }
                }


                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    Toast.makeText(context, t.toString(), Toast.LENGTH_LONG).show();
                    Log.i(TAG, "Failed" + t.toString());
                    data.setValue(null);
                }
            });
        }
        return data;
    }

}
