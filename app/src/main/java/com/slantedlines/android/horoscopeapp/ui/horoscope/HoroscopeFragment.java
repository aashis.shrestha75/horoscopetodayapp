package com.slantedlines.android.horoscopeapp.ui.horoscope;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.material.tabs.TabLayout;
import com.slantedlines.android.horoscopeapp.R;
import com.slantedlines.android.horoscopeapp.helper.Horoscope;
import com.slantedlines.android.horoscopeapp.helper.MyHoroscopeData;
import com.slantedlines.android.horoscopeapp.helper.MySessionManager;
import com.slantedlines.android.horoscopeapp.ui.login.DobActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class HoroscopeFragment extends Fragment implements HoroscopeRecyclerAdapter.OnHoroscopeSelectListener {
    private static final String TAG = "HoroscopeFragment";
    MySessionManager mySessionManager;
    AlertDialog dialog;
    NewViewPagerAdapter adapter;
    DashboardViewModel dashboardViewModel;
    ViewPager viewPager;
    TextView tvHoroscopeName, tvHoroscopeRange;
    ImageView imageHoroscopeSelect, imageViewSetting;
    private InterstitialAd mInterstitialAd;

    public HoroscopeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_horoscope, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvHoroscopeName = view.findViewById(R.id.tv_zodiac);
        tvHoroscopeRange = view.findViewById(R.id.tv_date_range);
        viewPager = view.findViewById(R.id.pager);
        TabLayout tabLayout = view.findViewById(R.id.tab_layout);
        imageViewSetting = view.findViewById(R.id.change_zodiac);
        imageHoroscopeSelect = view.findViewById(R.id.imgview_horoscope_image);
        dashboardViewModel = ViewModelProviders.of(this).get(DashboardViewModel.class);

        adapter = new NewViewPagerAdapter(getChildFragmentManager());
        mySessionManager = new MySessionManager(getContext());
        // attach tablayout with viewpager
        tabLayout.setupWithViewPager(viewPager);

        // add your fragments
        adapter.addFrag(new TodayFragment(), getString(R.string.today));
        adapter.addFrag(new YearlyFragment(), getString(R.string.yearly));

        // set adapter on viewpager
        viewPager.setAdapter(adapter);


        imageHoroscopeSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog();
            }
        });
        imageViewSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogChangeHoroscope();
            }
        });
        loadUI();
    }

    private void showDialogChangeHoroscope() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.change_horoscope));
        builder.setMessage(getString(R.string.ask_change_horoscope));
        builder.setNegativeButton(getString(R.string.cancel),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        builder.setPositiveButton(getString(R.string.string_change),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mySessionManager.setLogin(false);
                        mySessionManager.setTemporaryFetch(false);
                        mySessionManager.setHoroscope("");
                        dialog.cancel();
                        getActivity().finish();
                        Intent intent = new Intent(getActivity(), DobActivity.class);
                        startActivity(intent);
                    }
                });
        builder.show();
    }

    public void loadUI() {
        String horoscopeName;
        if (mySessionManager.getTemporaryFetch()) {
            horoscopeName = mySessionManager.getTemporaryHoros();
        } else {
            horoscopeName = mySessionManager.getHoroscope();
        }

        tvHoroscopeName.setText(horoscopeName);
        Horoscope horoscope = new MyHoroscopeData(getContext()).getMyHoroscope(horoscopeName);
        tvHoroscopeRange.setText(horoscope.getmHoroscopeDateRange());
        int resID = getResources().getIdentifier(horoscope.getmHoroscopeName().toLowerCase(), "drawable", getActivity().getPackageName());
        imageHoroscopeSelect.setImageResource(resID);
    }

    private void showDialog() {
        View mview = getLayoutInflater().inflate(R.layout.dialog_select_horoscope, null);
        final RecyclerView recyclerView = mview.findViewById(R.id.dialog_recyclerview);
        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(getContext(), R.style.AppTheme_NoActionBar));

        builder.setView(mview);
        dialog = builder.create();
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

//        dialog.setTitle(getString(R.string.select_horoscope));
        dialog.show();

        // use a linear layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

        // specify an adapter (see also next example)
        HoroscopeRecyclerAdapter mAdapter = new HoroscopeRecyclerAdapter(getContext(), new MyHoroscopeData(getContext()).getAllHoroscopes(), this);
        recyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onHoroscopeSelected(String horoscopeName) {
        dialog.dismiss();
        mySessionManager.setTemporaryHoros(horoscopeName);
        loadAd();
        loadUI();
    }

    private void loadAd() {
        mInterstitialAd = new InterstitialAd(getContext());
        mInterstitialAd.setAdUnitId(getString(R.string.change_horoscope_fullad));
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                mInterstitialAd.show();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                Log.e(TAG, "onAdFailedToLoad error code:" + errorCode);
                loadData();
            }

            @Override
            public void onAdClosed() {
                loadData();
            }
        });
    }

    private void loadData() {
        adapter.removeFrag();
        adapter.notifyDataSetChanged();
        // add your fragments
        adapter = new NewViewPagerAdapter(getChildFragmentManager());
        adapter.addFrag(new TodayFragment(), getString(R.string.today));
        adapter.addFrag(new YearlyFragment(), getString(R.string.yearly));
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mySessionManager.setTemporaryFetch(false);
    }
}
