package com.slantedlines.android.horoscopeapp.ui.quotes;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.slantedlines.android.horoscopeapp.R;
import com.slantedlines.android.horoscopeapp.model_horoscope.UrlWallpaper;
import com.squareup.picasso.Picasso;

import java.util.List;

public class StaggeredRecyclerAdapter extends RecyclerView.Adapter<StaggeredRecyclerAdapter.ViewHolder> {
    private static final String TAG = "StaggeredRecyclerAdapte";
    Context context;
    private List<UrlWallpaper> wallpapers;
    private OnImageClickListener listener;

    public StaggeredRecyclerAdapter(Context context, List<UrlWallpaper> wallpapers, OnImageClickListener onImageClickListener) {
        this.wallpapers = wallpapers;
        this.context = context;
        this.listener = onImageClickListener;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        Picasso.with(context)
                .load(wallpapers.get(position).getUrl())
                .placeholder(R.drawable.placeholder)
                .into(holder.imageView);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onImageClicked(wallpapers.get(position).getUrl());
            }
        });

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_grid_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    public interface OnImageClickListener {
        void onImageClicked(String imageUrl);
    }

    @Override
    public int getItemCount() {
        return wallpapers.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.img_quotes_gri);
        }
    }
}
