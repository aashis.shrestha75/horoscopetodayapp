package com.slantedlines.android.horoscopeapp.model_horoscope;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Yearly {

    @SerializedName("yearlyhoroscope")
    @Expose
    private List<YearlyHoroscope> yearlyhoroscope = null;

    public List<YearlyHoroscope> getYearlyhoroscope() {
        return yearlyhoroscope;
    }

    public void setYearlyhoroscope(List<YearlyHoroscope> yearlyhoroscope) {
        this.yearlyhoroscope = yearlyhoroscope;
    }

}