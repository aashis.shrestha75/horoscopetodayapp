package com.slantedlines.android.horoscopeapp.model_horoscope;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Urlyoutube {

    @SerializedName("url")
    @Expose
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}