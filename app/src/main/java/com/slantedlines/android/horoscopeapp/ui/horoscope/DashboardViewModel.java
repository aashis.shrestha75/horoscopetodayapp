package com.slantedlines.android.horoscopeapp.ui.horoscope;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.slantedlines.android.horoscopeapp.R;
import com.slantedlines.android.horoscopeapp.api.ApiClient;
import com.slantedlines.android.horoscopeapp.api.ApiService;
import com.slantedlines.android.horoscopeapp.helper.MySessionManager;
import com.slantedlines.android.horoscopeapp.model_horoscope.BirthDay;
import com.slantedlines.android.horoscopeapp.model_horoscope.BirthdayHoroscope;
import com.slantedlines.android.horoscopeapp.model_horoscope.Daily;
import com.slantedlines.android.horoscopeapp.model_horoscope.DailyHoroscope;
import com.slantedlines.android.horoscopeapp.model_horoscope.Yearly;
import com.slantedlines.android.horoscopeapp.model_horoscope.YearlyHoroscope;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardViewModel extends AndroidViewModel {

    private static final String TAG = "DashboardViewModel";
    DailyHoroscope dailyhoroscope = new DailyHoroscope();
    BirthdayHoroscope birthdayhoroscope = new BirthdayHoroscope();
    YearlyHoroscope yearlyHoroscope = new YearlyHoroscope();
    Context context;
    MySessionManager mySessionManager;
    private MutableLiveData<DailyHoroscope> mutableLiveDataDaily;
    private MutableLiveData<BirthdayHoroscope> mutableLiveDataBirthday;
    private MutableLiveData<YearlyHoroscope> mutableLiveDataYearly;

    public DashboardViewModel(@NonNull Application application) {
        super(application);
        this.context = application;
        this.mySessionManager = new MySessionManager(context);
        mutableLiveDataDaily = new MutableLiveData<>();
        mutableLiveDataBirthday = new MutableLiveData<>();
        mutableLiveDataYearly = new MutableLiveData<>();
    }

    public LiveData<DailyHoroscope> getDailyObservable() {
        return mutableLiveDataDaily;
    }


    public LiveData<BirthdayHoroscope> getBirthdayObservable() {
        return mutableLiveDataBirthday;
    }

    public LiveData<YearlyHoroscope> getYearlyObservable() {
        return mutableLiveDataYearly;
    }

    public void getDailyHoroscope(String todayDateSlug) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm.getActiveNetworkInfo() != null) {
            Call<Daily> call_Today = ApiClient.createService(ApiService.class, mySessionManager.getApiToken()).getDailyHoroscope(todayDateSlug);
            call_Today.enqueue(new Callback<Daily>() {
                @Override
                public void onResponse(Call<Daily> call, Response<Daily> response) {
                    if (response.isSuccessful()) {
                        if (response.body().getDailyhoroscope() != null && response.body().getDailyhoroscope().size() > 0) {
//                            Toast.makeText(context, response.body().getDailyhoroscope().get(0).getDailyhoroscope(), Toast.LENGTH_LONG).show();
                            dailyhoroscope = response.body().getDailyhoroscope().get(0);
                            Log.i(TAG, dailyhoroscope.toString());
                        } else {
                            Toast.makeText(context, context.getString(R.string.no_data), Toast.LENGTH_LONG).show();
                            dailyhoroscope.setDailyhoroscope(context.getString(R.string.no_data));
                        }
                        mutableLiveDataDaily.setValue(dailyhoroscope);

                    } else {
                        Toast.makeText(context, response.code() + "", Toast.LENGTH_SHORT).show();
                        Log.i(TAG, "onResponse code: " + response.code());
                        dailyhoroscope.setDailyhoroscope(context.getString(R.string.no_data));
                        mutableLiveDataDaily.setValue(dailyhoroscope);
                    }
                }


                @Override
                public void onFailure(Call<Daily> call, Throwable t) {
//                    Toast.makeText(context, t.toString(), Toast.LENGTH_LONG).show();
                    Log.e(TAG, "onFailure Daily: ", t);
                    dailyhoroscope.setDailyhoroscope(context.getString(R.string.no_data));
                    mutableLiveDataDaily.setValue(dailyhoroscope);
                }
            });
        } else {
            dailyhoroscope.setDailyhoroscope(context.getString(R.string.no_internet));
            mutableLiveDataDaily.setValue(dailyhoroscope);
            Toast.makeText(context, context.getString(R.string.no_internet), Toast.LENGTH_LONG).show();
        }
    }

    public void getBirthdayHoroscope(String todayDate) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm.getActiveNetworkInfo() != null) {
            Call<BirthDay> callBirthday = ApiClient.createService(ApiService.class, mySessionManager.getApiToken()).getBirthdayHoroscope(todayDate);
            callBirthday.enqueue(new Callback<BirthDay>() {
                @Override
                public void onResponse(Call<BirthDay> call, Response<BirthDay> response) {
                    if (response.isSuccessful()) {
                        if (response.body().getBirthdayhoroscope().size() > 0) {
                            birthdayhoroscope = response.body().getBirthdayhoroscope().get(0);
                            Log.i(TAG, birthdayhoroscope.toString());
                        } else {
                            Toast.makeText(context, context.getString(R.string.no_data), Toast.LENGTH_LONG).show();
                            birthdayhoroscope.setBirthdayhoroscope(context.getString(R.string.no_data));
                        }
                        mutableLiveDataBirthday.setValue(birthdayhoroscope);

                    } else {
                        Toast.makeText(context, response.code() + "", Toast.LENGTH_SHORT).show();
                        Log.i(TAG, "ResponseCode=" + response.code());
                        birthdayhoroscope.setBirthdayhoroscope(context.getString(R.string.no_data));
                        mutableLiveDataBirthday.setValue(birthdayhoroscope);
                    }
                }


                @Override
                public void onFailure(Call<BirthDay> call, Throwable t) {
//                    Toast.makeText(context, t.toString(), Toast.LENGTH_LONG).show();
                    Log.e(TAG, "onFailure Bday: ", t);
                    birthdayhoroscope.setBirthdayhoroscope(context.getString(R.string.no_data));
                    mutableLiveDataBirthday.setValue(birthdayhoroscope);
                }
            });
        } else {
            birthdayhoroscope.setBirthdayhoroscope(context.getString(R.string.no_internet));
            mutableLiveDataBirthday.setValue(birthdayhoroscope);
            Toast.makeText(context, context.getString(R.string.no_internet), Toast.LENGTH_LONG).show();
        }
    }

    public void getYearlyHoroscope(String year) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm.getActiveNetworkInfo() != null) {
            Call<Yearly> callYearly = ApiClient.createService(ApiService.class, mySessionManager.getApiToken()).getYearlyHoroscope(year);
            callYearly.enqueue(new Callback<Yearly>() {
                @Override
                public void onResponse(Call<Yearly> call, Response<Yearly> response) {
                    if (response.isSuccessful()) {
                        if (response.body().getYearlyhoroscope() != null && response.body().getYearlyhoroscope().size() > 0) {
                            yearlyHoroscope = response.body().getYearlyhoroscope().get(0);
                            Log.i(TAG, yearlyHoroscope.toString());
                        } else {
                            Toast.makeText(context, context.getString(R.string.no_data), Toast.LENGTH_LONG).show();
                            yearlyHoroscope.setYearlyhoroscope(context.getString(R.string.no_data));
                        }
                        mutableLiveDataYearly.setValue(yearlyHoroscope);

                    } else {
                        Toast.makeText(context, response.code() + "", Toast.LENGTH_SHORT).show();
                        Log.i(TAG, "ResponseCode=" + response.code());
                        yearlyHoroscope.setYearlyhoroscope(context.getString(R.string.no_data));
                        mutableLiveDataYearly.setValue(yearlyHoroscope);
                    }
                }


                @Override
                public void onFailure(Call<Yearly> call, Throwable t) {
//                    Toast.makeText(context, t.toString(), Toast.LENGTH_LONG).show();
                    Log.e(TAG, "onFailure Yearly: ", t);
                    yearlyHoroscope.setYearlyhoroscope(context.getString(R.string.no_data));
                    mutableLiveDataYearly.setValue(yearlyHoroscope);
                }
            });
        } else {
            yearlyHoroscope.setYearlyhoroscope(context.getString(R.string.no_internet));
            mutableLiveDataYearly.setValue(yearlyHoroscope);
            Toast.makeText(context, context.getString(R.string.no_internet), Toast.LENGTH_LONG).show();
        }
    }

}